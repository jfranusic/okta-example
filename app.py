import os

from flask import Flask
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask.ext.login import LoginManager
from flask.ext.login import current_user
from flask.ext.login import login_required
from flask.ext.login import login_user
from flask.ext.login import logout_user

from konfig import Konfig
from okta import Okta


app = Flask(__name__)
konf = Konfig()
app.secret_key = konf.secret_key

okta = Okta(konf.okta_token,
            konf.okta_org,
            konf.okta_base)

login_manager = LoginManager()
login_manager.setup_app(app)


class UserSession:
    def __init__(self, user_session_id):
        self.user_id = None
        self.active = False
        self.user_session_id = user_session_id
        okta.session.load(user_session_id)
        self.authenticated = okta.session.validate()
        if self.authenticated:
            self.user_id = okta.session.user_id
            user = okta.users.get(self.user_id)
            self.active = user['status'] == 'ACTIVE'

    def is_active(self):
        return self.active

    def is_authenticated(self):
        # "Has the user authenticated?"
        # See also: http://stackoverflow.com/a/19533025
        return self.authenticated

    def is_anonymous(self):
        return not self.authenticated

    def get_id(self):
        return self.user_session_id


@login_manager.user_loader
def load_user(user_session_id):
    return UserSession(user_session_id)


@app.route("/", methods=['GET', 'POST'])
def main_page():
    opts = {}
    if request.method == 'GET':
        return render_template('main_page.html', opts=opts)

    valid_user = okta.session.create(request.form['username'],
                                     request.form['password'])
    if not valid_user:
        opts['invalid_username_or_password'] = True
        return render_template('main_page.html', opts=opts)

    user = UserSession(okta.session.session_id)
    if not user.is_active():
        opts['account_not_active'] = True
        return render_template('main_page.html', opts=opts)

    login_user(user)
    url = url_for('logged_in', _external=True)
    set_okta_cookie = okta.session.cookie_redirect(url)
    return redirect(set_okta_cookie)


@app.route("/secret")
@login_required
def logged_in():
    opts = {'user': current_user,
            'okta_url': okta.organization_url()}
    return render_template('secret.html', opts=opts)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main_page'))

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    if port == 5000:
        app.debug = True
    app.run(host='0.0.0.0', port=port)
