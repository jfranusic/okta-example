import json
import urllib

import requests


class Okta:
    def __init__(self, token=None, organization=None, base='okta.com'):
        headers = {
            'Authorization': 'SSWS {}'.format(token),
            'Accept': 'application/json',
            'Content-Type': 'application/json'}
        base_url = 'https://{}.{}'.format(organization, base)
        self.base_url = base_url
        self.session = OktaSession(headers, base_url)
        self.users = OktaUsers(headers, base_url)

    def organization_url(self):
        return self.base_url


class OktaUsers:
    def __init__(self, headers, base_url):
        self.headers = headers
        self.base_url = base_url

    def get(self, user_id):
        url = '{}/api/v1/users/{}'.format(self.base_url, user_id)
        return requests.get(url, headers=self.headers).json()


class OktaSession:
    def __init__(self, headers, base_url):
        self.headers = headers
        self.base_url = base_url

        self.session_id = None
        self.user_id = None
        self.mfa_active = None
        self.cookie_token = None

    def load(self, session_id):
        self.session_id = session_id

    def cookie_redirect(self, redirect=None):
        if not self.cookie_token:
            return False
        redirect_url = urllib.quote_plus(redirect)
        template = ('{}/login/sessionCookieRedirect'
                    '?token={}&'
                    'redirectUrl={}')
        return template.format(self.base_url,
                               self.cookie_token,
                               redirect_url)

    def create(self, username, password):
        url = '{}/api/v1/sessions'.format(self.base_url)
        paramters = {
            'additionalFields': 'cookieToken'}
        payload = {
            'username': username,
            'password': password}

        r = requests.post(url,
                          headers=self.headers,
                          params=paramters,
                          data=json.dumps(payload))

        results = r.json()

        if 'cookieToken' in results:
            self.session_id = results['id']
            self.user_id = results['userId']
            self.mfa_active = results['mfaActive']
            self.cookie_token = results['cookieToken']
            return True
        else:
            return False

    def validate(self):
        template = '{}/api/v1/sessions/{}'
        url = template.format(self.base_url, self.session_id)
        results = requests.put(url, headers=self.headers).json()
        if 'userId' in results:
            self.user_id = results['userId']
            self.mfa_active = results['mfaActive']
            return True
        else:
            return False

    def close(self):
        template = '{}/api/v1/sessions/{}'
        url = template.format(self.base_url, self.session_id)
        results = requests.delete(url, headers=self.headers).json()
        if 'errorCode' in results:
            return False
        else:
            return True
